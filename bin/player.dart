import 'dart:io';

import 'game.dart';

class player extends game {
  @override
  void GameStart() {
    print('Welcome to the game');
    print('Please enter your name: ');
    playername = stdin.readLineSync()!;
    print('Your name is : ' + playername);
  }

  @override
  void MagicClass() {
    print('Your name is : ' + playername);
    print('Your class is Magic');
    print('Please enjoy the journey');
  }

  @override
  void SwordmanClass() {
    print('Your name is : ' + playername);
    print('Your class is Swordman');
    print('Please enjoy the journey');
  }

  @override
  void ArcherClass() {
    print('Your name is : ' + playername);
    print('Your class is Archer');
    print('Please enjoy the journey');
  }
}
