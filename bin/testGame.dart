import 'player.dart';
import 'dart:io';

void main(List<String> args) {
  player player1 = player();

  player1.GameStart();
  print('1: Magic');
  print('2: Swordman');
  print('3: Archer');
  print('Please select your class :');
  int select = int.parse(stdin.readLineSync()!);
  if (select == 1) {
    player1.MagicClass();
  } else if (select == 2) {
    player1.SwordmanClass();
  } else if (select == 3) {
    player1.ArcherClass();
  } else {
    player1.GameStart();
  }
}
